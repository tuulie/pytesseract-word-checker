import pytesseract as tess
import argparse
from PIL import Image
import re

# run with:
# /program_path python3 find_keywords.py --image /image_path --match "key_word"

ap = argparse.ArgumentParser()

ap.add_argument("-i", "--image",

                required=True,

                help="path to input image")

ap.add_argument("-m", "--match",

                required=True,

                help="target word or phrase")

args = vars(ap.parse_args())

testimg = tess.image_to_string(Image.open(args["image"]), lang="eng+fin")

match = args["match"]

if re.search(''+match+'', testimg):
    print("Match found.")

else:
    print("No match.")

